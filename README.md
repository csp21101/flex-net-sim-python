# Python Flex Net Sim

This simulator is a Python remake of [Flex Net Sim](https://gitlab.com/DaniloBorquez/flex-net-sim) which was done in C++.

## Objective

We understand that Python is an accessible language, so to facilitate the work of researchers we are developing this version focused on the accessibility of the simulator.

## Virtual environment

We use "[venv](https://docs.python.org/3/library/venv.html)" module to create the virtual enviroment.

To activate the virtual env (Windows):

```
simEnv\Scripts\activate.bat
```

## Collaborators

- Gonzalo España
- Danilo Borquéz
- Cristian Véliz
- Felipe Falcón

## License

[MIT](https://choosealicense.com/licenses/mit/)
