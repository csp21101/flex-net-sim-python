# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1] - 2021-12-15

### Fixed

- Removing slot decorator (property and setter) because they were using multiple parameters.
- Fixing getSlot() method (does not exist)

### Added

- Testing to negatives ID and length in Link class.
- Testing number of slots setter
- Testing link ednpoints
- Controller Class

## [0.1.0] - 2021-12-09

### Added

- Connection Class
- Testing:
  - add_link from vector
  - add_link with from-to method
  - exceptions
- Connection class documentation
- Class Node and Link

[0.1.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.1.0...v0.1.1
